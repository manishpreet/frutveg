package com.example.hp.fruitsorder;

import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

class MyAdapter2 extends RecyclerView.Adapter<MyAdapter2.Holder> {
    Context context;
    Vegetables vegetables;
    ArrayList<Cart>list;
    public MyAdapter2(Context context, Vegetables vegetables) {
        this.context=context;
        this.vegetables=vegetables;
        list=new ArrayList<>();
    }

    @NonNull
    @Override
    public MyAdapter2.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view,viewGroup,false);
        return new Holder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final MyAdapter2.Holder holder, final int i) {
        /*Glide.with(context)
                .load(vegetables.images[i])
                .into(holder.imageView);*/
        Picasso.with(context).load(vegetables.images[i]).placeholder(R.drawable.dummy).into(holder.imageView);
        holder.discount.setText(vegetables.discount[i]);
        holder.name.setText(vegetables.names[i]);
        holder.dec.setText(vegetables.description[i]);
        holder.actualprice.setText(vegetables.price2[i]);
        holder.quantity.setText(vegetables.quantity[i]);
        final String price=getPrice(vegetables.price2[i],vegetables.discount[i].replace("% Off",""));
        holder.discountprice.setText(price);
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cart cart= new  Cart();
                cart.setImage(vegetables.images[i]);
                cart.setPrice(vegetables.price[i]);
                cart.setDes(vegetables.description[i]);
                cart.setCount(0);
                cart.setName(vegetables.names[i]);
                cart.setImage(vegetables.images[i]);
                if (holder.button.getText().equals("ADD")){
                    list.add(cart);
                    holder.button.setText("REMOVE");
                }else
                {
                    Util.showProgress(context);
                    int index=getIndex(cart.getName());
                    if (index>-1) {
                        list.remove(index);
                        holder.button.setText("ADD");
                    }
                    Util.hideProgress();
                }
                /*Intent intent;
                intent = new Intent(context, VegetableSellers.class);
                context.startActivity(intent);*/
            }
        });
    }
    private String getPrice(String price, String discount) {
        int pri=Integer.parseInt(price.replace("Rs.",""));
        int dis=Integer.parseInt(discount);
        int totalPrice=pri-(pri*dis)/100;
        String rs="Rs."+totalPrice;
        return rs;
    }
    private int getIndex(String name) {

        for (int i=0;i<list.size();i++)
        {
            if (list.get(i).getName().equals(name))
                return i;
            else
                return -1;
        }
        return -1;
    }
    @Override
    public int getItemCount() {
        return vegetables.images.length;
    }

    public class Holder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView name;
        TextView dec;
        TextView discountprice;
        TextView quantity;
        TextView actualprice;
        TextView discount;
        TextView button;
        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.itemImage);
            name=itemView.findViewById(R.id.name);
            actualprice =itemView.findViewById(R.id.strikethrough);
            dec=itemView.findViewById(R.id.description);
            discountprice =itemView.findViewById(R.id.price);
            quantity=itemView.findViewById(R.id.quantiy);
            button=itemView.findViewById(R.id.add);
            actualprice.setPaintFlags(actualprice.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);
            discount=itemView.findViewById(R.id.discount);
        }
    }
}
