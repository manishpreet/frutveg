package com.example.hp.fruitsorder;

import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
                if (user==null)
                {
                    Intent intent=new Intent(SplashScreen.this,Loginpage.class);
                    startActivity(intent);
                    finish();
                }else
                {
                    Intent intent=new Intent(SplashScreen.this,Homepage.class);
                    startActivity(intent);
                    finish();
                }
            }
        },1000);

    }
}


