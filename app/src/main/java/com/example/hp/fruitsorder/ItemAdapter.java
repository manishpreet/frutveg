package com.example.hp.fruitsorder;



import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.Holder> {
    Context context;
    Fruits fruits;
    ArrayList<Cart>list;
    ItemAdapter(Context context, Fruits fruits){
        this.context=context;
        this.fruits=fruits;
        list=new ArrayList<>();
    }
    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view,viewGroup,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, final int i) {
     // Glide.with(context).load(fruits.images[i]).into(holder.imageView);
        Picasso.with(context).load(fruits.images[i]).placeholder(R.drawable.dummy).into(holder.imageView);
        holder.discount.setText(fruits.discount[i]);
        holder.name.setText(fruits.names[i]);
        holder.dec.setText(fruits.description[i]);
        holder.actualPrice.setText(fruits.pricea[i]);
        final String price=getPrice(fruits.pricea[i],fruits.discount[i].replace("% Off",""));
        holder.discountPrice.setText(price);
        holder.quantity.setText(fruits.quantity[i]);
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cart cart=new Cart();
                cart.setImage(fruits.images[i]);
                cart.setName(fruits.names[i]);
                cart.setCount(1);
                cart.setQuantity(fruits.quantity[i]);
                cart.setDes(fruits.description[i]);
                cart.setPrice(price);
               if (holder.button.getText().equals("ADD"))
               {
                   list.add(cart);
                   holder.button.setText("REMOVE");
               }else
               {
                   Util.showProgress(context);
                   int index=getIndex(cart.getName());
                   if (index>-1) {
                       list.remove(index);
                       holder.button.setText("ADD");
                   }
                   Util.hideProgress();
               }

               /* Intent intent;
                intent = new Intent(context,FruitSellers.class);
                context.startActivity(intent);*/
            }

        });
    }

    private String getPrice(String price, String discount) {
        int pri=Integer.parseInt(price.replace("Rs.",""));
        int dis=Integer.parseInt(discount);
        int totalPrice=pri-(pri*dis)/100;
        String rs="Rs."+totalPrice;
        return rs;
    }

    private int getIndex(String name) {

        for (int i=0;i<list.size();i++)
        {
            if (list.get(i).getName().equals(name))
                return i;
            else
                return -1;
        }
        return -1;
    }

    @Override
    public int getItemCount() {
        return fruits.images.length;
    }

    public class Holder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView name;
        TextView dec;
        TextView discountPrice;
        TextView actualPrice;
        TextView discount;
        TextView quantity;
        TextView button;
        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.itemImage);
            name=itemView.findViewById(R.id.name);
            button=itemView.findViewById(R.id.add);
            actualPrice =itemView.findViewById(R.id.strikethrough);
            dec=itemView.findViewById(R.id.description);
            quantity=itemView.findViewById(R.id.quantiy);
            discount=itemView.findViewById(R.id.discount);
            discountPrice =itemView.findViewById(R.id.price);
            actualPrice.setPaintFlags(actualPrice.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }
}
