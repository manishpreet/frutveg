package com.example.hp.fruitsorder;

import android.app.ProgressDialog;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Loginpage extends AppCompatActivity {
    Button login;
    EditText username;
    EditText password;
    TextView fpassword;
    TextView newuser;
    String u;
    String p;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginpage);
        username=findViewById(R.id.etusername);
        newuser=findViewById(R.id.tvnew);
        password=findViewById(R.id.etpassword);
        fpassword=findViewById(R.id.fpassword);
        login=findViewById(R.id.blogin);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()){
                    doLogin();
                }

            }
        });
        newuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Loginpage.this,Registration_page.class);
                startActivity(intent);
            }
        });
        fpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent=new Intent(Loginpage.this,Forgotpassword.class);
                startActivity(intent);
            }
        });
    }

    public void doLogin(){
        Util.showProgress(this);
        FirebaseAuth.getInstance().signInWithEmailAndPassword(u,p)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Util.hideProgress();
                        if(task.isSuccessful()) {
                            Toast.makeText(Loginpage.this, "LoginSuccessful", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(Loginpage.this,Homepage.class);
                            startActivity(intent);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Util.hideProgress();
                Toast.makeText(Loginpage.this,e.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }
    public boolean validate(){
        u = username.getText().toString();
        p = password.getText().toString();
        if(u.isEmpty()||p.isEmpty())
            return false;
        else
            return true;
    }
}
