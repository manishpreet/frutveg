package com.example.hp.fruitsorder;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import java.util.Date;

public class CartPage extends AppCompatActivity {
RecyclerView recyclerView;
    CartAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_page);
        String data=getIntent().getStringExtra("data");
        CartList cartList=new Gson().fromJson(data,CartList.class);
        recyclerView=findViewById(R.id.recycler1);

        adapter = new CartAdapter(this,cartList);
        RecyclerView.LayoutManager manager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);

        findViewById(R.id.order)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Util.showProgress(CartPage.this);
                        Booking booking=new Booking();
                        booking.setUid(FirebaseAuth.getInstance().getUid());
                        booking.setTimestemp(new Date().getTime());
                        booking.setItems(adapter.cartList.getList());

                        FirebaseFirestore.getInstance().collection("orders")
                                .document().set(booking)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful())
                                        {
                                            Toast.makeText(CartPage.this, "Order Placed Succesfully", Toast.LENGTH_SHORT).show();
                                            Util.hideProgress();
                                            Intent intent=new Intent(CartPage.this,Homepage.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                            startActivity(intent);
                                            finish();
                                        }
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Util.hideProgress();
                                Toast.makeText(CartPage.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
    }

}
