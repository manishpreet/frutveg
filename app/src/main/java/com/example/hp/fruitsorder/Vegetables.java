package com.example.hp.fruitsorder;

class Vegetables {
    String[] images={
            "http://pngimg.com/uploads/garlic/garlic_PNG12802.png",
            "https://www.stickpng.com/assets/images/5b4eed0cc051e602a568ce0c.png",
            "http://www.pngpix.com/wp-content/uploads/2016/03/Fresh-Chili-PNG-image.png",
            "https://www.freepngimg.com/thumb/tomato/6-tomato-png-image-thumb.png",
            "http://pluspng.com/img-png/onion-png-onion-png-image-788.png",
            "http://www.pngall.com/wp-content/uploads/2016/04/Potato-PNG-Picture.png",
            "https://www.freepngimg.com/thumb/ladyfinger/42356-1-lady-finger-free-download-png-hd-thumb.png",
             "https://ya-webdesign.com/transparent450_/green-pumpkin-png-2.png",
            "http://www.pngmart.com/files/7/Mooli-PNG-Transparent-Image.png",
            "https://www.pngarts.com/files/4/Carrot-PNG-Download-Image.png",
            "https://purepng.com/public/uploads/large/purepng.com-bitter-gourdvegetables-bitter-melon-bitter-gourd-momordica-charantia-bitter-squash-balsam-pear-941524726197qszna.png",
            "http://vicamart.com/wp-content/uploads/2017/10/ridge-gourd_burned.png",
            "https://vegify.in/wp-content/uploads/2019/02/1549113837291.png",
            "https://www.hapurhub.com/wp-content/uploads/2019/01/local-vegetable-capsicum-green-hybrid-v-250-g-3.png",
            "https://cdn.pixabay.com/photo/2017/07/09/05/36/vegetable-2486289_1280.png",
            "https://www.redsunfarms.com/img/product/pack1/YellowPepperbulk.png?v=1.0",
            "https://stickeroid.com/uploads/pic/full-pngmart/37c73939bbda1f6fb2692b78e89e710d9009383c.png",
            "http://pngimg.com/uploads/cauliflower/cauliflower_PNG12686.png",
            "https://www.pngarts.com/files/1/Cabbage-PNG-Background-Image.png",
            "https://purepng.com/public/uploads/large/purepng.com-brinjalvegetables-brinjal-eggplant-melongene-garden-egg-guinea-squash-941524725891tf1xf.png",
            "http://pngimg.com/uploads/beet/beet_PNG52.png"
    };
    String[] quantity={"-250 gm","-250 gm","-250 gm","-1Kg","-1Kg","-1Kg","-1Kg","-1Kg", "-1Kg","-1Kg","-1Kg","-1Kg","=1Kg","-1Kg","-1Kg","-1Kg",
            "-1Kg","-1kg","-1Kg", "-1Kg", "-1Kg"};
    String[] names={"Garlic-250 gm","Ginger-250 gm","Green Chillis-250 gm","Tomato-1Kg","Onion-1Kg","Potato-1Kg","Ladyfinger-1Kg","Pumpkin-1Kg",
            "Raddish-1Kg","Carrot-1Kg","BitterGourd-1Kg","RidgeGourd-1Kg","BottleGourd-1Kg","Capsicum-1Kg","Red Bell Pepper-1Kg","Yellow Bell Pepper-1Kg",
            "Cucumber-1Kg","Caulflower-1kg","Cabbage-1Kg", "EggPlant-1Kg", "BeetRoot-1Kg"
    };
    String[] price2={"Rs.45","Rs.20","Rs.15","Rs.65","Rs.60","Rs.30","Rs.60","Rs.30","Rs.40","Rs.55","Rs.55","Rs.45","Rs.40","Rs.70","Rs.200","Rs.200",
            "Rs.40","Rs.45","Rs.20","Rs.40","Rs.50"};
    String[] price={"Rs.30","Rs.10","Rs.10","Rs.50","Rs.40","Rs.20","Rs.50","Rs.25","Rs.30","Rs.44","Rs.45","Rs.35","Rs.30","Rs.55","Rs.150","Rs.150",
            "Rs.30","Rs.32","Rs.12","Rs.30","Rs.30"};
    String[] description={"Lasun","Adrak","Hari Mirch","Tamatar","Pyaaz","Aloo","Bhindi","Petha","Mooli","Gajar","Krela","Tori","Lauki",
            "Phadi Mirch","Red Bell","Yellow Bell","Kheera","Pattagobi","Gobbi","Bengan","Chukandar"};
    String[] discount={"20% Off","30% Off","10% Off","25% Off","50% Off","60% Off","20% Off","30% Off","10% Off","55% Off","10% Off"
            ,"45% Off","10% Off","35% Off","10% Off","40% Off","20% Off","30% Off","35% Off","30% Off","35% Off"};
}
