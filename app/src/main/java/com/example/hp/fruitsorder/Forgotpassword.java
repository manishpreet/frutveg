package com.example.hp.fruitsorder;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class Forgotpassword extends AppCompatActivity {
EditText mail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpassword);
        mail=findViewById(R.id.etusername);
        findViewById(R.id.blogin)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String email=mail.getText().toString();
                        if (email.isEmpty())
                            Toast.makeText(Forgotpassword.this, "Enter E-mail", Toast.LENGTH_SHORT).show();
                        doForgot(email);
                    }
                });
    }

    private void doForgot(String email) {
        Util.showProgress(this);
        FirebaseAuth.getInstance().sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful())
                        {
                            Util.hideProgress();
                            Toast.makeText(Forgotpassword.this, "Check MailBox To Reset Passwors", Toast.LENGTH_SHORT).show();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Util.hideProgress();
                Toast.makeText(Forgotpassword.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
